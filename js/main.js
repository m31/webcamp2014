jQuery(function ($) {

    //region ========== Init carousel ==========
    $(".partners-carousel")
        .jcarousel({
            wrap: "circular"
        })
        .jcarouselAutoscroll({
            interval: 2500,
            target: '+=1',
            autostart: true
        });

    $(".jcarousel-prev").jcarouselControl({
        target: "-=1"
    });

    $(".jcarousel-next").jcarouselControl({
        target: "+=1"
    });
    //endregion

    //region ========== Scroll to the top of the page ==========
    var $htmlBody = $("html,body");

    $(".upward").on("click", function (e) {
        e.preventDefault();

        $htmlBody.animate({
            scrollTop: 0
        }, "slow");
    });
    //endregion

    //region ===== Animate scroll to block =====
    $(".main-nav a").on('click', function(e) {
      if (this.hash.length > 0) {
        e.preventDefault();
        $htmlBody.animate({
          scrollTop: $(this.hash).offset().top - 67
        }, "slow");
      }
    });
    //endregion

    //region ========== Animate navigation on scroll ==========
    var $hiddenCloneHeader = $(".clone-header");

    $(window).on("scroll", function () {
        var windowScrollPosition = $(window).scrollTop();

        if (windowScrollPosition > 200) {
            $hiddenCloneHeader.css({
                top: 0
            });
        } else {
            $hiddenCloneHeader.css({
                top: -100
            });
        }
    });
    //endregion

    //region ========== Google map ==========
    var g = google.maps;

    function initialize() {
        var myLatlng = new g.LatLng(
                46.4817763,
                30.7473057
            ),
            mapOptions = {
                zoom: 18,
                center: myLatlng,
                panControl: false,
                zoomControl: false,
                mapTypeControl: false,
                scaleControl: false,
                streetViewControl: false,
                overviewMapControl: false,
                scrollwheel: false,
                navigationControl: false,
                draggable: false
            },
            map = new g.Map(
                document.getElementById("map"),
                mapOptions
            ),
            image = new g.MarkerImage("img/sprite.png",
                new g.Size(25, 25),
                new g.Point(0, 52)
            ),
            marker = new g.Marker({
                position: myLatlng,
                map: map,
                icon: image,
                title: "ул. Греческая, 1а"
            });
    }

    g.event.addDomListener(window, "load", initialize);
    //endregion

});